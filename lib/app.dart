import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:localizations/bloc/app_control_cubit.dart';
import 'package:localizations/service/preferences_service.dart';
import 'package:localizations/ui/home_page.dart';
import 'package:localizations/l10n/generated/app_localizations.dart';

class App extends StatelessWidget {
  static Widget create(PreferencesService prefs) {
    final initialControlState = AppControlState(
      locale: prefs.getLocale(),
    );
    return BlocProvider<AppControlCubit>(
      create: (_) => AppControlCubit(
        prefs,
        initialControlState,
      ),
      child: const App._(),
    );
  }

  const App._({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppControlCubit, AppControlState>(
        builder: (context, state) {
      return MaterialApp(
        home: const HomePage(),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        locale: state.locale,
      );
    });
  }
}
