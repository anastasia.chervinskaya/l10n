import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:localizations/bloc/app_control_cubit.dart';
import 'package:localizations/l10n/generated/app_localizations.dart';
import 'package:localizations/l10n/localization_helper.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AppControlCubit getCubit(BuildContext context) {
    return context.read<AppControlCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: DropdownButtonHideUnderline(
          child: DropdownButton<Locale>(
            icon: const Icon(Icons.language),
            items: AppLocalizations.supportedLocales
                .map((locale) => DropdownMenuItem(
                      value: locale,
                      child: Text(locale.languageCode),
                    ))
                .toList(),
            onChanged: (locale) {
              if (locale == null) return;
              getCubit(context).setLocale(locale);
            },
          ),
        ),
      ),
      body: Center(
        child: Text(getLocalizations(context).goodEvening),
      ),
    );
  }
}
