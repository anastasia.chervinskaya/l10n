import 'package:flutter/material.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:localizations/app.dart';
import 'package:localizations/service/preferences_service.dart';

void main() async {
  await Hive.initFlutter();

  final prefs = await PreferencesService.getInstance();

  runApp(App.create(prefs));
}
